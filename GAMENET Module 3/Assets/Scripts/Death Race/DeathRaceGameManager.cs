using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using System.Linq;

public class DeathRaceGameManager : MonoBehaviourPunCallbacks
{
    public static DeathRaceGameManager instance = null;

    public GameObject[] vehiclePrefabs;
    public Transform[] startingPositions;
    public Transform AnnouncementParent;
    public Text PlayersLeftText;

    [Header("Game Over Panel")]
    public GameObject WinnerPanel;
    public Text WinnerNameText;

    private int disconnectedPlayers;
    public Dictionary<int, string> Players = new Dictionary<int, string>();
    public Dictionary<int, Camera> PlayerCams = new Dictionary<int, Camera>();

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        disconnectedPlayers = 0;

        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = startingPositions[actorNumber - 1].position;
                PhotonNetwork.Instantiate(vehiclePrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity);
            }
        }
    }

    public void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.EliminatedCode)
        {
            object[] data = (object[])photonEvent.CustomData;

            //string deadPlayer = (string)data[0];
            string killer = (string)data[1];
            int killerId = (int)data[2];
            int deadId = (int)data[3];

            if (Players.ContainsKey(deadId))
            {
                Players.Remove(deadId);
                PlayerCams.Remove(deadId);

                string tmp = "PLAYER'S LEFT: " + (Players.Count - disconnectedPlayers).ToString();
                foreach (KeyValuePair<int, string> player in Players)
                {
                    tmp += "\n" + player.Value;
                }

                PlayersLeftText.text = tmp;

                if (Players.Count - disconnectedPlayers < 2)
                {
                    WinnerPanel.SetActive(true);
                    WinnerNameText.text = killer;
                }
            }
        }
    }

    public void LeaveGame()
    {
        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("LobbyScene");
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        disconnectedPlayers++;

        string tmp = "PLAYER'S LEFT: " + (Players.Count - disconnectedPlayers).ToString();
        foreach (KeyValuePair<int, string> player in Players)
        {
            tmp += "\n" + player.Value;
        }

        PlayersLeftText.text = tmp;

        if (Players.Count - disconnectedPlayers < 2)
        {
            WinnerPanel.SetActive(true);
            WinnerNameText.text = Players.ElementAt(0).Value;
        }
    }

}
