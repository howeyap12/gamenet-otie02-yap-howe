using UnityEngine;

public enum Faction { Player, Monster, Building }
public class Team : MonoBehaviour
{
    [SerializeField]
    private Faction myFaction;
    public Faction MyFaction
    {
        get
        {
            return myFaction;
        }
    }
}
