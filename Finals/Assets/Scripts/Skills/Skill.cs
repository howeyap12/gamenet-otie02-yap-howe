using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class Skill : MonoBehaviour
{
    public event Action<float> SkillActivated;

    protected Unit unit;

    [SerializeField]
    private Sprite skillIcon;
    public Sprite SkillIcon
    {
        get
        {
            return skillIcon;
        }
    }

    [SerializeField]
    private float cooldown;

    protected bool IsOnCooldown;

    private void Awake()
    {
        unit = GetComponentInParent<Unit>();
    }
    public virtual void Activate()
    {
        SkillActivated.Invoke(cooldown);
        StartCoroutine(StartCooldown());
    }

    public IEnumerator StartCooldown()
    {
        IsOnCooldown = true;
        yield return new WaitForSeconds(cooldown);
        IsOnCooldown = false;
    }
}
