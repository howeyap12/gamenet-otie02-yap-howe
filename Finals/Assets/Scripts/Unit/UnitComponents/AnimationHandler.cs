using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : MonoBehaviour
{
    public Animator MyAnimator { get; private set; }

    private Unit unit;

    private void Awake()
    {
        MyAnimator = GetComponent<Animator>();
        unit = GetComponent<Unit>();
    }

    private void Update()
    {
        HandleLayers();
    }

    public void HandleLayers()
    {
        if (unit.IsMoving)
        {
            ActivateLayer("WalkLayer");
            MyAnimator.SetFloat("MoveX", unit.MoveInput.x);
            MyAnimator.SetFloat("MoveY", unit.MoveInput.y);
        }
        else if (unit.IsAttacking)
        {
            ActivateLayer("AttackLayer");
        }
        else
        {
            ActivateLayer("IdleLayer");
        }

        //if (unit.IsAlive)
        //{
        //}
        //else
        //{
        //    ActivateLayer("DeathLayer");
        //}
    }

    public void ActivateLayer(string layerName)
    {
        for (int i = 0; i < MyAnimator.layerCount; i++)
        {
            MyAnimator.SetLayerWeight(i, 0);
        }

        MyAnimator.SetLayerWeight(MyAnimator.GetLayerIndex(layerName), 1);
    }
}
