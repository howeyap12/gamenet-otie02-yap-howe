public enum RaiseEventsCode
{
    ProjectileDestroyCode = 1
}
public enum Heroes
{
    Asclepius = 0,
    Hercules = 0
}

public class Constants
{
    public const string PLAYER_READY = "isPLayerReady";
    public const string PLAYER_SELECTION_NUMBER = "playerSelectionNumber";
}
