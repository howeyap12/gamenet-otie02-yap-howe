using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class Health : MonoBehaviourPunCallbacks
{
    [Header("HP Related Stuff")]
    public float startHealth = 100;
    public Image healthBar;

    private float health;
    private bool isDead;

    private void Start()
    {
        health = startHealth;
        this.healthBar.fillAmount = health / startHealth;

    }

    [PunRPC]
    public void TakeDamage(int damage, int killerId, PhotonMessageInfo info)
    {
        if (isDead) return;

        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            isDead = true;

            GetComponent<PlayerSetup>().camera.transform.parent = null;
            GetComponent<VehicleMovement>().enabled = false;

            Die(info, killerId);
        }
    }

    public void Die(PhotonMessageInfo info, int killerId)
    {

        string killerName = info.Sender.NickName;
        string deadName = photonView.Owner.NickName;
        int deadId = photonView.ViewID;

        // event data
        object[] data = new object[] { deadName, killerName, killerId, deadId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions()
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte)RaiseEventsCode.EliminatedCode, data, raiseEventOptions, sendOptions);
    }
}
