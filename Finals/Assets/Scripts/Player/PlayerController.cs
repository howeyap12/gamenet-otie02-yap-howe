using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    [SerializeField]
    private GameObject touchParticle;

    [SerializeField]
    private LayerMask touchLayerMask;

    private Unit unit;

    private Vector3 origisad;

    private void Awake()
    {
        unit = GetComponent<Unit>();
    }

    public void Attack()
    {
        unit.Attacking.Attack();
    }

    public void Update()
    {
        HandleJoystick();

        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                if (IsPointerOverUI(0)) return;

                Ray touchposition = Camera.main.ScreenPointToRay(new Vector3(touch.position.x, touch.position.y, 0f));
                //RaycastHit2D hit = Physics2D.GetRayIntersection(touchposition, 100.0f, touchLayerMask.value);
                RaycastHit2D hit = Physics2D.CircleCast(touchposition.origin, 1.0f, touchposition.direction, 100f, touchLayerMask);
                origisad = touchposition.origin;
                if (hit)
                {
                    Unit targetUnit = hit.collider.GetComponent<Unit>();
                    if (targetUnit)
                    {
                        unit.UIManager.Deselect();
                        targetUnit.Select();
                        unit.MyTarget = targetUnit;
                    }
                }
                else
                {
                    unit.UIManager.Deselect();
                }
            }
        }
    }
    void OnDrawGizmosSelected()
    {
        // Draw a yellow sphere at the transform's position
        Gizmos.color = Color.yellow;
        Gizmos.DrawSphere(origisad, 1.5f);
    }

    public bool IsPointerOverUI(int fingerId)
    {
        EventSystem eventSystem = EventSystem.current;
        return (eventSystem.IsPointerOverGameObject(fingerId)
            && eventSystem.currentSelectedGameObject != null);
    }

    public void HandleJoystick()
    {
        float horizontalAxis = UltimateJoystick.GetHorizontalAxis("Movement");
        float verticalAxis = UltimateJoystick.GetVerticalAxis("Movement");

        unit.Movement.MoveInput = new Vector2(horizontalAxis, verticalAxis);
    }
}
