using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Header("Spawn References")]

    [SerializeField]
    private Transform[] SpawnPoints;

    private void Awake()
    {
        SingletonManager.Register(this);
    }

    public Vector3 GetRandomSpawnPoint()
    {
        return SpawnPoints[Random.Range(0, SpawnPoints.Length)].position;
    }
}
