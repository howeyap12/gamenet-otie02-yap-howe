using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : Unit
{
    protected override void Start()
    {
        base.Start();
        UIManager.OnDeselect += RemoveTarget;
    }

    private void RemoveTarget()
    {
        MyTarget = null;
    }
}
