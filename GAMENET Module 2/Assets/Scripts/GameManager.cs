using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;
using TMPro;
using ExitGames.Client.Photon;
using Photon.Realtime;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourPunCallbacks
{
    public event Action<PhotonMessageInfo> OnKillFeedChange;
    public event Action OnEndGame;

    public GameObject playerPrefab;

    [Header("Kill Feed")]
    public Transform KillFeedParent;

    [Header("Leaderboards")]
    public Transform LeaderboardParent;
    public GameObject scorePrefab;
    public GameObject winnerOverlay;
    public TextMeshProUGUI winnerText;

    private Dictionary<string, int> leaderboard;
    private Dictionary<string, TextMeshProUGUI> playerScoreTexts;


    private void Awake()
    {
        SingletonManager.Register(this);
    }

    void Start()
    {
        leaderboard = new Dictionary<string, int>();

        if (PhotonNetwork.IsConnectedAndReady)
        {
            PhotonView pv = PhotonNetwork.Instantiate(playerPrefab.name, SingletonManager.Get<SpawnManager>().GetRandomSpawnPoint(), Quaternion.identity).GetComponent<PhotonView>();
            //photonView.RPC("NewPlayer", RpcTarget.AllBuffered, pv.Owner.NickName);
            //NewPlayer(pv.Owner.NickName);
        }
    }

    [PunRPC]
    public void NewPlayer(string playerName)
    {
        leaderboard.Add(playerName, 0);
        playerScoreTexts.Add(playerName, Instantiate(scorePrefab, LeaderboardParent).GetComponentInChildren<TextMeshProUGUI>());
        playerScoreTexts[playerName].text = playerName + ": 0k";
    }

    public void AddScore(PhotonMessageInfo info)
    {
        if (leaderboard.ContainsKey(info.Sender.NickName))
        {
            leaderboard[info.Sender.NickName] = leaderboard[info.Sender.NickName] + 1;
            //photonView.RPC("UpdateLeaderboards", RpcTarget.AllBuffered, info.Sender.NickName);

            if (leaderboard[info.Sender.NickName] >= 10)
            {
                Debug.Log(info.Sender.NickName + " WINS!");
                winnerText.text = info.Sender.NickName;
                GameEnd();
                //photonView.RPC("GameEnd", RpcTarget.AllBuffered);
            }
        }
        else
        {
            //Debug.LogWarning("Player Not Found");
            leaderboard.Add(info.Sender.NickName, 1);
        }
    }

    [PunRPC]
    public void UpdateLeaderboards(string playerName)
    {
        leaderboard[playerName] = leaderboard[playerName] + 1;
        playerScoreTexts[playerName].text = leaderboard[playerName].ToString() + ": " + leaderboard[playerName] + "k";
    }

    [PunRPC]
    public void GameEnd()
    {
        StartCoroutine(GameEndRoutine());
    }

    public IEnumerator GameEndRoutine()
    {
        winnerOverlay.SetActive(true);

        yield return new WaitForSeconds(5.0f);

        PhotonNetwork.LeaveRoom();
        PhotonNetwork.CurrentRoom.IsOpen = false;
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("LobbyScene");
    }

    // Is Dead didn't false on other client
    // How to get photonnetwork instantiated object
    // Can GameManager use an RPC?
    // PhotonMessageInfo get component
}
