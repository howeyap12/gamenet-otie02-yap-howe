using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class ShootingProjectile : Shoot
{
    [Header("Shoot Config")]
    [SerializeField]
    private Transform[] firePoints;

    [SerializeField]
    private GameObject bullet;

    [SerializeField]
    private int damage;

    private int currentFirePoint;

    private void Start()
    {
        currentFirePoint = 1;
        canFire = true;
    }

    public override void Fire()
    {
        photonView.RPC("InstantiateBullet", RpcTarget.All);
    }

    [PunRPC]
    public void InstantiateBullet()
    {
        Quaternion rot = transform.rotation * Quaternion.Euler(90f, 0, 0);
        Projectile proj = Instantiate(bullet, firePoints[currentFirePoint].position, rot).GetComponent<Projectile>();
        proj.OnHitPlayer += DamageEnemy;
        proj.MyId = photonView.ViewID;
        currentFirePoint = currentFirePoint == 1 ? 0 : 1;
    }

    public void DamageEnemy(PhotonView view)
    {
        view.RPC("TakeDamage", RpcTarget.AllBuffered, damage, photonView.ViewID);
    }
}
