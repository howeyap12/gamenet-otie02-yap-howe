using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class BuildingManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject testTower;

    [SerializeField]
    private Transform towerPlacement;
    public void Build()
    {
        photonView.RPC("BuildRPC", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void BuildRPC()
    {
        PhotonView pv = PhotonNetwork.Instantiate(testTower.name, towerPlacement.position, Quaternion.identity).GetComponent<PhotonView>();
    }
}
