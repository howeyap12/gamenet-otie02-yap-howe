using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class MarchState : IState
{
    private Enemy parent;

    public void Enter(Enemy parent)
    {
        this.parent = parent;
    }

    public void Exit()
    {
        parent.Movement.MoveInput = Vector2.zero;
    }

    public void Update()
    {
        parent.Movement.MoveInput = (parent.Waypoints[parent.CurrentWaypointIteration].position - parent.transform.position).normalized;

        float distance = Vector2.Distance(parent.Waypoints[parent.CurrentWaypointIteration].position, parent.transform.position);

        if (distance <= 1.0f)
        {
            if (parent.Waypoints.Length - 1 > parent.CurrentWaypointIteration)
            {
                parent.CurrentWaypointIteration++;
            }
            else
            {
                parent.ChangeState(new IdleState());
            }
        }

        if (parent.MyTarget != null)
        {
            parent.ChangeState(new FollowState());
        }
    }
}

