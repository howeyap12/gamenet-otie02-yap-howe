using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject playerUiPrefab;

    [SerializeField]
    private Camera playerCamera;

    [SerializeField]
    private TextMeshProUGUI playerNameText;

    private PlayerController playerController;

    private Unit unit;
    private void Awake()
    {
        playerController = GetComponent<PlayerController>();
        unit = GetComponent<Unit>();
    }
    // Start is called before the first frame update
    void Start()
    {
        playerNameText.text = photonView.Owner.NickName;
        GameManager gm = SingletonManager.Get<GameManager>();

        if (photonView.IsMine)
        {
            playerController.enabled = true;
            playerCamera.enabled = true;
            PlayerUI playerUi = Instantiate(playerUiPrefab).GetComponent<PlayerUI>();
            for (int i = 0; i < unit.SkillManager.Skills.Length; i++)
            {
                //Change naming later
                UISkill ui = playerUi.InstantiateSkillUI();
                ui.SetSkill(unit.SkillManager.Skills[i]);
            }
            playerUi.BuildButton.onClick.AddListener(() => GetComponent<BuildingManager>().Build());

            playerUi.AttackButton.onClick.AddListener(() => playerController.Attack());
            Instantiate(playerCamera);
            gm.VirtualCamera.Follow = transform;
        }
        else
        {
            playerController.enabled = false;
            playerCamera.enabled = false;
        }
    }
}
