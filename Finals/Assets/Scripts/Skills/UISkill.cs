using UnityEngine;
using UnityEngine.UI;

public class UISkill : MonoBehaviour
{
    [SerializeField]
    private Button myButton;

    [SerializeField]
    private Image skillIcon;

    [SerializeField]
    private Image cooldownIndicator;

    private float timer;
    private float cdTime;

    public void SetSkill(Skill skill)
    {
        skillIcon.sprite = skill.SkillIcon;
        skill.SkillActivated += StartCooldown;
        myButton.onClick.AddListener(() => skill.Activate());

        cdTime = 1;
    }

    private void Update()
    {
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }

        cooldownIndicator.fillAmount = timer / cdTime;
    }

    public void StartCooldown(float time)
    {
        cdTime = time;
        timer = time;
    }
}
