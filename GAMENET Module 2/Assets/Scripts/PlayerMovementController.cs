using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class PlayerMovementController : MonoBehaviour
{
    public FixedTouchField fixedTouchField;

    private RigidbodyFirstPersonController rigidbodyFirstPersonController;

    private Animator animator;

    private void Start()
    {
        rigidbodyFirstPersonController = this.GetComponent<RigidbodyFirstPersonController>();
        animator = this.GetComponent<Animator>();
    }

    public void FixedUpdate()
    {
        float horizontalAxis = UltimateJoystick.GetHorizontalAxis("Movement");
        float verticalAxis = UltimateJoystick.GetVerticalAxis("Movement");

        animator.SetFloat("horizontal", horizontalAxis);
        animator.SetFloat("vertical", verticalAxis);

        if (Mathf.Abs(horizontalAxis) > 0.9 || Mathf.Abs(verticalAxis) > 0.9)
        {
            animator.SetBool("isRunning", true);
            rigidbodyFirstPersonController.movementSettings.ForwardSpeed = 8;
        }
        else
        {
            animator.SetBool("isRunning", false);
            rigidbodyFirstPersonController.movementSettings.ForwardSpeed = 4;
        }

        rigidbodyFirstPersonController.joystickInputAxis.x = horizontalAxis;
        rigidbodyFirstPersonController.joystickInputAxis.y = verticalAxis;

        if (fixedTouchField != null)
        {
            rigidbodyFirstPersonController.mouseLook.lookInputAxis = fixedTouchField.TouchDist;
        }
    }
}
