using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public Button AttackButton;
    public GameObject SkillPrefab;
    public Transform SkillsParent;
    public GameObject BuildingsParent;
    public Button BuildButton;
    public void OpenBuildings()
    {
        BuildingsParent.SetActive(BuildingsParent.gameObject.activeSelf ? false : true);
    }

    public UISkill InstantiateSkillUI()
    {
        UISkill ui = Instantiate(SkillPrefab, SkillsParent).GetComponent<UISkill>();

        return ui;
    }
}
