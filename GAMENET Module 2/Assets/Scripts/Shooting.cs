using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;
using System;

public class Shooting : MonoBehaviourPunCallbacks
{
    public event Action<PhotonMessageInfo> OnDeath;

    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP Related Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthBar;

    [Header("Kill Feed Related")]
    public Transform KillFeedParent;
    public GameObject KillPrefab;
    public TextMeshProUGUI ScoreText;

    private Animator animator;

    private bool isDead;
    private int score;

    // Start is called before the first frame update
    void Start()
    {
        score = 0;
        health = startHealth;
        healthBar.fillAmount = health / startHealth;
        animator = this.GetComponent<Animator>();

        this.KillFeedParent = SingletonManager.Get<GameManager>().KillFeedParent;
        OnDeath += SingletonManager.Get<GameManager>().AddScore;
    }

    public void Fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if (Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25);
                //hit.collider.gameObject.GetComponent<Shooting>().OnDeath += AddScore;
            }
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        if (isDead) return;

        this.health -= damage;
        this.healthBar.fillAmount = health / startHealth;

        if (health <= 0)
        {
            isDead = true;
            Die();
            OnDeath?.Invoke(info);
            GameObject killPrefab = Instantiate(KillPrefab, KillFeedParent);
            killPrefab.transform.SetSiblingIndex(0);
            killPrefab.transform.Find("KillerText").GetComponent<TextMeshProUGUI>().text = info.Sender.NickName;
            killPrefab.transform.Find("KilledText").GetComponent<TextMeshProUGUI>().text = info.photonView.Owner.NickName;
            Destroy(killPrefab, 3);

            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName);
        }
    }

    public void AddScore(PhotonMessageInfo info)
    {
        if (info.Sender.UserId == photonView.Owner.UserId)
        {
            score++;
            ScoreText.text = "KILLS: " + score;
        }
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, 0.2f);
    }

    public void Die()
    {
        if (photonView.IsMine)
        {
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }
    }

    IEnumerator RespawnCountdown()
    {
        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while (respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawning in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        this.transform.position = SingletonManager.Get<SpawnManager>().GetRandomSpawnPoint();
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        isDead = false;
        health = 100;
        healthBar.fillAmount = health / startHealth;
    }
}
