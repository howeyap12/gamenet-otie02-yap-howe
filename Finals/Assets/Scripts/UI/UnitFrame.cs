using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnitFrame : MonoBehaviour
{
    [Header("General")]

    [SerializeField]
    private TextMeshProUGUI nameText;

    [SerializeField]
    private Image healthBar;

    [SerializeField]
    private Image manaBar;

    [Header("Heroes Only")]

    [SerializeField]
    private Image portrait;

    [SerializeField]
    private TextMeshProUGUI playerNameText;

    private Unit selectedUnit;

    public void SelectUnit(Unit unit)
    {
        selectedUnit = unit;
        nameText.text = unit.UnitName;
        manaBar.transform.parent.gameObject.SetActive(selectedUnit.HasMana);
        HandleBars();
        unit.HealthComponent.OnHealthChanged += HandleBars;
    }
    public void SelectUnit(Unit unit, string playerName)
    {
        portrait.sprite = unit.Icon;
        selectedUnit = unit;
        nameText.text = unit.UnitName;
        playerNameText.text = playerName;
        manaBar.transform.parent.gameObject.SetActive(selectedUnit.HasMana);
        HandleBars();
        unit.HealthComponent.OnHealthChanged += HandleBars;
    }

    public void HandleBars()
    {
        if (selectedUnit != null)
        {
            healthBar.fillAmount = selectedUnit.HealthComponent.MyCurrentValue / selectedUnit.HealthComponent.MyMaxValue;

            if (selectedUnit.HasMana)
            {
                manaBar.fillAmount = selectedUnit.ManaComponent.MyCurrentValue / selectedUnit.ManaComponent.MyMaxValue;
            }
        }
    }
}
