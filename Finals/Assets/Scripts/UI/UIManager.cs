using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class UIManager : MonoBehaviour
{
    public event Action OnDeselect;

    [SerializeField]
    private UnitFrame playerFrame;

    [SerializeField]
    private UnitFrame targetFrame;

    [SerializeField]
    private Transform partyParent;

    private void Awake()
    {
        SingletonManager.Register(this);
    }

    public void SelectTarget(Unit unit)
    {
        targetFrame.gameObject.SetActive(true);
        targetFrame.SelectUnit(unit);
        unit.HealthComponent.OnDeath += Deselect;
    }

    public void Deselect()
    {
        OnDeselect?.Invoke();
        targetFrame.gameObject.SetActive(false);
    }

    public void SetPlayerFrame(Unit unit, string playerName)
    {
        playerFrame.SelectUnit(unit, playerName);
    }
}
