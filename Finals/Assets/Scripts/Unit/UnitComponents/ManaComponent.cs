using UnityEngine;
using Photon.Pun;
using System;

public class ManaComponent : MonoBehaviourPunCallbacks
{
    public float MyMaxValue { get; set; }

    public float MyCurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (value > MyMaxValue)
            {
                currentValue = MyMaxValue;
            }
            else if (value < 0)
            {
                currentValue = 0;
            }
            else
            {
                currentValue = value;
            }
        }
    }
    [SerializeField]
    private float maxMana;

    private float currentValue;

    private void Start()
    {
        MyMaxValue = maxMana;
        MyCurrentValue = maxMana;
    }

    [PunRPC]
    public void UseMana(int manaCost)
    {
        MyCurrentValue -= manaCost;
    }
}
