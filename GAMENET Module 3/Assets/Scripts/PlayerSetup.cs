using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    public Camera camera;

    [Header("Death Race")]
    [SerializeField]
    private Text playerNameText;
    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();
        GetComponent<VehicleMovement>().enabled = photonView.IsMine;
        camera.enabled = photonView.IsMine;
        if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<LapController>().enabled = photonView.IsMine;
        }
        else if (PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr"))
        {
            GetComponent<Shoot>().enabled = photonView.IsMine;
            GetComponent<Death>().enabled = photonView.IsMine;
            playerNameText.text = photonView.Owner.NickName;
            photonView.RPC("AddMyself", RpcTarget.AllBuffered);
        }
    }

    [PunRPC]
    public void AddMyself()
    {
        DeathRaceGameManager DRGM = DeathRaceGameManager.instance;
        if (!DRGM.Players.ContainsKey(photonView.ViewID))
        {
            DRGM.Players.Add(photonView.ViewID, photonView.Owner.NickName);
            DRGM.PlayerCams.Add(photonView.ViewID, camera);

            string tmp = "PLAYER'S LEFT: " + DRGM.Players.Count.ToString();
            foreach (KeyValuePair<int, string> player in DRGM.Players)
            {
                tmp += "\n" + player.Value;
            }

            DRGM.PlayersLeftText.text = tmp;
        }
    }
}
