using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PortalSpawner : MonoBehaviour
{
    [SerializeField]
    private Transform[] waypoints;

    [SerializeField]
    private float spawnRate;

    [SerializeField]
    private GameObject monster;

    private void Start()
    {
        StartCoroutine(SpawnRoutine());
    }

    private IEnumerator SpawnRoutine()
    {
        while (true)
        {
            yield return new WaitForSeconds(spawnRate);

            Spawn();

            yield return null;
        }
    }

    public void Spawn()
    {
        if (PhotonNetwork.IsMasterClient)
        {
            PhotonView pv = PhotonNetwork.Instantiate(monster.name, transform.position, Quaternion.identity).GetComponent<PhotonView>();
            Enemy enemy = pv.GetComponent<Enemy>();
            enemy.SetWaypoints(waypoints);
        }
    }
}
