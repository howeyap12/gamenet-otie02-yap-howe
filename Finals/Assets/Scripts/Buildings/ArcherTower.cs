using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArcherTower : Unit
{
    public float MyAttackTime { get; set; }

    [SerializeField]
    private float attackSpeed;

    [SerializeField]
    private BuildingCollider buildingCollider;

    protected override void Start()
    {
        base.Start();
        buildingCollider.EnemyEntered += SetTarget;
    }

    public void SetTarget(Unit target)
    {
        if (MyTarget == null)
        {
            MyTarget = target;
        }
    }
    private void Update()
    {
        if (!IsAttacking)
        {
            MyAttackTime += Time.deltaTime;

            if (MyAttackTime >= attackSpeed && MyTarget != null)
            {
                MyAttackTime = 0;

                Attacking.Attack();
            }
        }
    }
}
