using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;

public class PlayerListItemInitializer : MonoBehaviour
{
    [Header("UI References")]
    public Text PlayerNameText;
    //public Text ReadyText;
    public Image PlayerReadyImage;

    private bool isPlayerReady = false;

    public void Initialize(int playerId, string playerName, HeroSelectionManager heroSelectionManager)
    {
        PlayerNameText.text = playerName;

        if (PhotonNetwork.LocalPlayer.ActorNumber != playerId)
        {
        }
        else
        {
            // sets custom property for each player "isPlayerready"
            ExitGames.Client.Photon.Hashtable intializeProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady } };
            PhotonNetwork.LocalPlayer.SetCustomProperties(intializeProperties);
            heroSelectionManager.HeroSelected += OnHeroSelected;
        }
    }

    public void SetPlayerReady(bool playerReady)
    {
        PlayerReadyImage.enabled = playerReady;

        if (playerReady)
        {
            //ReadyText.text = "Ready";
        }
        else
        {
            //ReadyText.text = "";
        }
    }

    public void OnHeroSelected()
    {
        isPlayerReady = true;
        SetPlayerReady(isPlayerReady);

        ExitGames.Client.Photon.Hashtable newProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_READY, isPlayerReady } };

        PhotonNetwork.LocalPlayer.SetCustomProperties(newProperties);
    }
}
