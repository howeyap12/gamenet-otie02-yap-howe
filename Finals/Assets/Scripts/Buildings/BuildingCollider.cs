using UnityEngine;
using System;

public class BuildingCollider : MonoBehaviour
{
    public event Action<Unit> EnemyEntered;

    [SerializeField]
    private Faction target;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.gameObject.GetComponent<Unit>();

        if (unit)
        {
            if (unit.Team.MyFaction == target)
            {
                EnemyEntered?.Invoke(unit);
            }
        }
    }
}
