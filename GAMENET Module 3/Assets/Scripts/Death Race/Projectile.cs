using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Photon.Pun;

public class Projectile : MonoBehaviour
{
    public event Action<PhotonView> OnHitPlayer;

    private Rigidbody rb;

    [SerializeField]
    private float speed;

    public int MyId;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player") && other.GetComponent<PhotonView>().ViewID != MyId)
        {
            OnHitPlayer?.Invoke(other.GetComponent<PhotonView>());
            Destroy(gameObject);
        }
        else if (other.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        rb.velocity = transform.up * speed;
    }
}
