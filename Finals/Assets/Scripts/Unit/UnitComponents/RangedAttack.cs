using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class RangedAttack : Attacking
{
    [SerializeField]
    private GameObject projectile;

    [SerializeField]
    private Transform attackPoint;

    private Dictionary<int, Projectile> projectiles = new Dictionary<int, Projectile>();
    private int syncedId;
    private Projectile currentProjectile;
    private Transform projectileDump;
    private void Start()
    {
        projectileDump = SingletonManager.Get<GameManager>().ProjectileDump;
    }

    public override void Attack()
    {
        photonView.RPC("SpawnProjectile", RpcTarget.All);
    }

    [PunRPC]
    public void SpawnProjectile()
    {
        currentProjectile = Instantiate(projectile, attackPoint.position, attackPoint.rotation, projectileDump).GetComponent<Projectile>();
        if (photonView.IsMine)
        {
            if (unit.MyTarget != null && unit.MyTarget.Team.MyFaction == Faction.Monster)
            {
                currentProjectile.SetTarget(unit.MyTarget.gameObject, true);
            }
            currentProjectile.OnHit += OnEnemyHit;
            currentProjectile.SetCanMove(true);
            photonView.RPC("SyncId", RpcTarget.All, currentProjectile.GetInstanceID());
        }

        currentProjectile.OnDestroyProjectile += RemoveProjectile;
        photonView.ObservedComponents.Add(currentProjectile);
    }
    public void OnEnemyHit(PhotonView enemy)
    {
        unit.MyTarget = enemy.GetComponent<Unit>();
        unit.MyTarget.HealthComponent.OnDeath += NullTarget;

        enemy.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, attackDamage, unit.Team.MyFaction);
    }

    public void RemoveProjectile(int id)
    {
        if (photonView.IsMine)
        {
            photonView.RPC("CallRemoveProjectile", RpcTarget.All, id);
        }
    }

    [PunRPC]
    public void SyncId(int id)
    {
        syncedId = id;
        currentProjectile.name = "projectile " + syncedId.ToString();
        projectiles.Add(syncedId, currentProjectile);
    }

    [PunRPC]
    public void CallRemoveProjectile(int id)
    {
        if (projectiles.ContainsKey(id))
        {
            photonView.ObservedComponents.Remove(projectiles[id]);
            Destroy(projectiles[id].gameObject);
            projectiles.Remove(id);
        }
    }

    public void NullTarget()
    {
        unit.MyTarget = null;
    }
}
