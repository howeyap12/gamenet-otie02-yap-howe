using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using TMPro;
using System;

public class HeroSelectionManager : MonoBehaviour
{
    public event Action HeroSelected;

    [SerializeField]
    private TextMeshProUGUI heroNameText;

    [SerializeField]
    private GameObject selectButton;

    [SerializeField]
    private string[] heroNames;

    private int currentHero;
    public void SelectHero()
    {
        ExitGames.Client.Photon.Hashtable playerSelectionProperties = new ExitGames.Client.Photon.Hashtable() { { Constants.PLAYER_SELECTION_NUMBER, currentHero } };
        PhotonNetwork.LocalPlayer.SetCustomProperties(playerSelectionProperties);

        HeroSelected?.Invoke();
    }

    public void HighlightHero(int heroIndex)
    {
        currentHero = heroIndex;
        heroNameText.text = heroNames[heroIndex];
        selectButton.gameObject.SetActive(true);
    }
}
