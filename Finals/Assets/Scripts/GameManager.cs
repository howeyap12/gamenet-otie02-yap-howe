using System.Collections;
using UnityEngine;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Cinemachine;

public class GameManager : MonoBehaviourPunCallbacks
{
    public Transform ProjectileDump;
    public Transform DamageDump;

    public PlayerSpawnManager SpawnManager;

    public CinemachineVirtualCamera VirtualCamera;

    [SerializeField]
    private GameObject[] heroPrefabs;

    private UIManager UIMan;

    private void Awake()
    {
        SingletonManager.Register(this);
    }

    void Start()
    {
        UIMan = SingletonManager.Get<UIManager>();

        if (PhotonNetwork.IsConnectedAndReady)
        {
            object playerSelectionNumber;

            if (PhotonNetwork.LocalPlayer.CustomProperties.TryGetValue(Constants.PLAYER_SELECTION_NUMBER, out playerSelectionNumber))
            {
                Debug.Log((int)playerSelectionNumber);
                int actorNumber = PhotonNetwork.LocalPlayer.ActorNumber;
                Vector3 instantiatePosition = SpawnManager.GetPlayerSpawnPoint(PhotonNetwork.LocalPlayer.ActorNumber);
                PhotonView pv = PhotonNetwork.Instantiate(heroPrefabs[(int)playerSelectionNumber].name, instantiatePosition, Quaternion.identity).GetComponent<PhotonView>();
                UIMan.SetPlayerFrame(pv.GetComponent<Unit>(), pv.Owner.NickName);
            }
        }
    }

    [PunRPC]
    public void GameEnd()
    {
        StartCoroutine(GameEndRoutine());
    }

    public IEnumerator GameEndRoutine()
    {
        yield return new WaitForSeconds(5.0f);

        PhotonNetwork.LeaveRoom();
    }

    public override void OnLeftRoom()
    {
        SceneManager.LoadScene("LobbyScene");
    }
}
