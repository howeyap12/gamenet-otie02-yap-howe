using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using System.Linq;

public enum RaiseEventsCode
{
    EliminatedCode = 0
}
public class Death : MonoBehaviourPunCallbacks
{
    [Header("Announcement Related")]
    public GameObject TextPrefab;
    private Transform textParent;

    private int recentlyKilled;
    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    private void Start()
    {
        textParent = DeathRaceGameManager.instance.AnnouncementParent;
    }

    void OnEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventsCode.EliminatedCode)
        {

            object[] data = (object[])photonEvent.CustomData;

            string deadPlayer = (string)data[0];
            string killer = (string)data[1];
            int killerId = (int)data[2];
            int deadId = (int)data[3];

            if (recentlyKilled == deadId) return;
            recentlyKilled = deadId;

            Text newText = Instantiate(TextPrefab, textParent).GetComponent<Text>();

            if (killerId == photonView.ViewID)
            {
                newText.text = string.Format("<color=green>{0}</color>(YOU) eliminated <color=red>{1}</color>", killer, deadPlayer);
            }
            else if (deadId == photonView.ViewID)
            {
                newText.text = string.Format("<color=red>{0}</color> eliminated <color=green>{1}</color>(YOU)", killer, deadPlayer);
            }
            else
            {
                newText.text = string.Format("<color=red>{0}</color> eliminated <color=red>{1}</color>", killer, deadPlayer);
            }

            if (deadId == photonView.ViewID)
            {
                GetComponent<PlayerSetup>().camera.enabled = false;
                DeathRaceGameManager DGMR = DeathRaceGameManager.instance;
                this.enabled = false;
                Destroy(gameObject, 2.0f);
            }
        }
    }
}
