using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public abstract class Shoot : MonoBehaviourPunCallbacks
{
    protected bool canFire;
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && canFire)
        {
            Fire();
        }
    }

    public abstract void Fire();


}
