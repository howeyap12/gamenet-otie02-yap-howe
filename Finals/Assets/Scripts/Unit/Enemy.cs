using UnityEngine;

public class Enemy : Unit
{

    private IState currentState;

    public bool InRange
    {
        get
        {
            return Vector2.Distance(transform.position, MyTarget.transform.position) < MyAggroRange;
        }
    }
    public float MyAttackRange
    {
        get
        {
            return attackRange;
        }
        set
        {
            value = attackRange;
        }
    }

    public Transform[] Waypoints { get; private set; }
    public float MyAttackTime { get; set; }
    public float MyAggroRange { get; set; }

    [SerializeField]
    private float attackRange;

    [SerializeField]
    private float aggroRange;

    [SerializeField]
    private float attackSpeed;

    public int CurrentWaypointIteration { get; set; }

    protected override void Awake()
    {
        base.Awake();
        CurrentWaypointIteration = 0;
        MyAggroRange = aggroRange;
        ChangeState(new IdleState());
    }
    private void Update()
    {
        if (!IsAttacking)
        {
            MyAttackTime += Time.deltaTime;
        }
        currentState.Update();
    }

    public void SetWaypoints(Transform[] wps)
    {
        Waypoints = wps;
        ChangeState(new MarchState());
    }


    public void ChangeState(IState newState)
    {
        if (currentState != null)
        {
            currentState.Exit();
        }

        currentState = newState;

        currentState.Enter(this); //reference myself and enter new state
    }
    public void SetTarget(Unit target)
    {
        if (MyTarget == null)
        {
            float distance = Vector2.Distance(transform.position, target.transform.position);
            MyAggroRange = aggroRange;
            MyAggroRange += distance;
            MyTarget = target;
        }
    }
}
