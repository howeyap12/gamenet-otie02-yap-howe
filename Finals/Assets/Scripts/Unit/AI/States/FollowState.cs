using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

class FollowState : IState
{
    private Enemy parent;

    public void Enter(Enemy parent)
    {
        this.parent = parent;
    }

    public void Exit()
    {
        parent.Movement.MoveInput = Vector2.zero;
    }

    public void Update()
    {
        if (parent.MyTarget != null)
        {
            parent.Movement.MoveInput = (parent.MyTarget.transform.position - parent.transform.position).normalized;

            float distance = Vector2.Distance(parent.MyTarget.transform.position, parent.transform.position);

            if (distance <= parent.MyAttackRange)
            {
                parent.ChangeState(new AttackState());
            }
        }
        if (!parent.InRange)
        {
            parent.ChangeState(new MarchState());
        }
    }
}

