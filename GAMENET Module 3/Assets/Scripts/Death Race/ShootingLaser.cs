using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class ShootingLaser : Shoot
{
    [Header("Laser Settings")]
    public LineRenderer laserLineRenderer;
    public float laserWidth = 0.1f;
    public float laserMaxLength = 5f;

    [SerializeField]
    private Transform firePoint;

    private Health health;

    private void Start()
    {
        health = GetComponent<Health>();
        Vector3[] initLaserPositions = new Vector3[2] { Vector3.zero, Vector3.zero };
        laserLineRenderer.SetPositions(initLaserPositions);
        laserLineRenderer.SetWidth(laserWidth, laserWidth);
        canFire = true;
    }

    public override void Fire()
    {
        Ray ray = new Ray(firePoint.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 200))
        {
            photonView.RPC("DrawLaser", RpcTarget.All);

            if (hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 25, photonView.ViewID);
            }
        }
    }

    [PunRPC]
    public void DrawLaser()
    {
        Ray ray = new Ray(firePoint.position, transform.forward);
        RaycastHit raycastHit;
        Vector3 endPosition = firePoint.position + (laserMaxLength * transform.forward);

        if (Physics.Raycast(ray, out raycastHit, laserMaxLength))
        {
            endPosition = raycastHit.point;
        }

        laserLineRenderer.SetPosition(0, firePoint.position);
        laserLineRenderer.SetPosition(1, endPosition);
        StartCoroutine(OnOffRoutine());
    }

    private void OnOffLaser(bool isActivated)
    {
        laserLineRenderer.enabled = isActivated;
    }

    private IEnumerator OnOffRoutine()
    {
        OnOffLaser(true);
        canFire = false;
        yield return new WaitForSeconds(0.5f);
        OnOffLaser(false);
        canFire = true;
    }
}
