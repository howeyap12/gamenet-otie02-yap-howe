using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System;

public class Projectile : MonoBehaviour, IPunObservable
{
    public event Action<PhotonView> OnHit;
    public event Action<int> OnDestroyProjectile;

    [SerializeField]
    private float projectileSpeed;

    private Rigidbody2D rb;

    private GameObject target;

    private bool canMove;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        Invoke("StartDestroyEvent", 5.0f);
    }

    private void FixedUpdate()
    {
        if (canMove)
        {
            Move();
        }
    }

    public void SetTarget(GameObject newTarget, bool isOwner)
    {
        this.target = newTarget;
        newTarget.GetComponent<HealthComponent>().OnDeath += LostTarget;
        canMove = isOwner;
    }

    public void SetCanMove(bool isOwner)
    {
        canMove = isOwner;
    }

    public void LostTarget()
    {
        StartDestroyEvent();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (target)
        {
            if (target == collision.gameObject)
            {
                OnHit?.Invoke(collision.GetComponent<PhotonView>());
                target.GetComponent<HealthComponent>().OnDeath -= LostTarget;
                StartDestroyEvent();
            }
        }
        else
        {
            Team unit = collision.GetComponent<Team>();
            if (unit)
            {
                if (unit.MyFaction == Faction.Monster)
                {
                    OnHit?.Invoke(collision.GetComponent<PhotonView>());
                    StartDestroyEvent();
                }
            }
        }
    }

    private void Move()
    {
        if (target)
        {
            Vector2 tester = target.transform.position - transform.position;
            float rot_z = Mathf.Atan2(tester.y, tester.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
            rb.velocity = transform.up * projectileSpeed;
        }
        else
        {
            rb.velocity = transform.up * projectileSpeed;
        }
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }

        else if (stream.IsReading)
        {
            transform.position = (Vector3)stream.ReceiveNext();
            transform.rotation = (Quaternion)stream.ReceiveNext();
        }
    }

    public void StartDestroyEvent()
    {
        OnDestroyProjectile?.Invoke(GetInstanceID());
    }
}
