using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour
{
    private Enemy parent;

    private void Start()
    {
        parent = GetComponentInParent<Enemy>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Unit unit = collision.gameObject.GetComponent<Unit>();

        if (unit)
        {
            if (unit.Team.MyFaction == Faction.Player || unit.Team.MyFaction == Faction.Building)
            {
                parent.SetTarget(unit);
            }
        }
    }
}
