using UnityEngine;
using TMPro;

public class DamagePopup : MonoBehaviour
{
	[SerializeField]
	private float speed;

	[SerializeField]
	private TextMeshProUGUI displayNumber;

	// Update is called once per frame
	void Update()
	{
		transform.position = new Vector3(transform.position.x, transform.position.y + (speed * Time.deltaTime), transform.position.z);
	}

	public void Initialize(int damage, Color newColor)
    {
		displayNumber.text = "" + damage;
		displayNumber.color = newColor;
		Destroy(gameObject, 1.0f);
	}
}
