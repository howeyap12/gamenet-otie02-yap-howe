using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerMeleeAttack : Attacking
{
    [SerializeField]
    private float attackRadius;

    public LayerMask enemyLayer;

    public override void Attack()
    {
        if (!unit.IsAttacking)
        {
            StartCoroutine(AttackLoad());
            unit.AnimationHandler.MyAnimator.SetTrigger("Attack");
            Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, attackRadius); // add layermask later on

            foreach (Collider2D enemy in hitColliders)
            {
                Team enemyTeam = enemy.GetComponent<Team>();
                if (enemyTeam)
                {
                    if (enemyTeam.MyFaction == Faction.Monster)
                    {
                        enemy.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, attackDamage, unit.Team.MyFaction);
                    }
                }
            }
        }
    }

    private IEnumerator AttackLoad()
    {
        unit.IsAttacking = true;
        yield return new WaitForSeconds(1.0f);
        unit.IsAttacking = false;
    }
}
