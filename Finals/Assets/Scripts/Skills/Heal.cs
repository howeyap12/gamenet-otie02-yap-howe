using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class Heal : Skill
{
    [SerializeField]
    private int healValue;

    public override void Activate()
    {
        if (IsOnCooldown) return;

        if (unit.MyTarget)
        {
            if (unit.MyTarget.Team.MyFaction == Faction.Player)
            {
                base.Activate();

                unit.MyTarget.GetComponent<PhotonView>().RPC("Heal", RpcTarget.AllBuffered, healValue);
            }
        }
    }
}
