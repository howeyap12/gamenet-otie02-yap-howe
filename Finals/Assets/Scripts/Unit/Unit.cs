using UnityEngine;

public class Unit : MonoBehaviour
{
    public Sprite Icon
    {
        get
        {
            return icon;
        }
    }

    public SkillManager SkillManager
    {
        get
        {
            return skillManager;
        }
    }

    public Unit MyTarget { get; set; }
    public UIManager UIManager { get; private set; }
    public string UnitName { get; private set; }
    public Attacking Attacking { get; private set; }
    public Team Team { get; private set; }
    public HealthComponent HealthComponent { get; private set; }
    public AnimationHandler AnimationHandler { get; private set; }
    public ManaComponent ManaComponent { get; private set; }
    public Movement Movement { get; private set; }
    public bool HasMana { get; private set; }
    public bool IsAttacking { get; set; }
    public bool IsMoving
    {
        get
        {
            return Movement.MoveInput.x != 0 || Movement.MoveInput.y != 0;
        }
    }
    public Vector2 MoveInput
    {
        get
        {
            return Movement.MoveInput;
        }
    }
    public bool IsAlive { get; set; }


    [SerializeField]
    private Sprite icon;

    [SerializeField]
    private string unitName;

    [SerializeField]
    private SpriteRenderer selectionCircle;

    [SerializeField]
    private SkillManager skillManager;
    protected virtual void Awake()
    {
        UnitName = unitName;
        Attacking = GetComponent<Attacking>();
        Team = GetComponent<Team>();
        HealthComponent = GetComponent<HealthComponent>();
        ManaComponent = GetComponent<ManaComponent>();
        Movement = GetComponent<Movement>();
        AnimationHandler = GetComponent<AnimationHandler>();
        HasMana = ManaComponent != null;
    }

    protected virtual void Start()
    {
        UIManager = SingletonManager.Get<UIManager>();
    }

    public void Select()
    {
        UIManager.SelectTarget(this);
        selectionCircle.color = Color.white;
        UIManager.OnDeselect += Deselect;
    }

    public void Deselect()
    {
        UIManager.OnDeselect -= Deselect;
        selectionCircle.color = Color.clear;
    }
}
