using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawnManager : MonoBehaviour
{
    [SerializeField]
    private Transform[] playerSpawns;

    public Vector3 GetPlayerSpawnPoint(int actorNumber)
    {
        return playerSpawns[actorNumber-1].position;
    }
}
