using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class MeleeAttack : Attacking
{
    [SerializeField]
    private float attackRadius;

    public LayerMask enemyLayer;

    public override void Attack()
    {
        unit.AnimationHandler.MyAnimator.SetTrigger("Attack");
        Collider2D[] hitColliders = Physics2D.OverlapCircleAll(transform.position, attackRadius); // add layermask later on

        foreach (Collider2D enemy in hitColliders)
        {
            Team enemyTeam = enemy.GetComponent<Team>();
            if (enemyTeam)
            {
                if (enemyTeam.MyFaction == Faction.Building)
                {
                    enemy.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, attackDamage, unit.Team.MyFaction);
                }

                if (enemyTeam.MyFaction == Faction.Player)
                {
                    enemy.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, attackDamage, unit.Team.MyFaction);
                }
            }
        }
    }
}
