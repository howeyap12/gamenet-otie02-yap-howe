using UnityEngine;
using Photon.Pun;
using System;

public class HealthComponent : MonoBehaviourPunCallbacks
{
    public event Action OnHealthChanged;
    public event Action OnDeath;

    public float MyMaxValue { get; set; }

    public float MyCurrentValue
    {
        get
        {
            return currentValue;
        }
        set
        {
            if (value > MyMaxValue)
            {
                currentValue = MyMaxValue;
            }
            else if (value < 0)
            {
                currentValue = 0;
            }
            else
            {
                currentValue = value;
            }
        }
    }
    [SerializeField]
    private float maxHealth;

    [SerializeField]
    private DamagePopup damagePopupPrefab;

    private float currentValue;
    private Unit unit;
    private Transform damageDump;

    private void Awake()
    {
        unit = GetComponent<Unit>();
        MyMaxValue = maxHealth;
        MyCurrentValue = maxHealth;
    }

    private void Start()
    {
        damageDump = SingletonManager.Get<GameManager>().DamageDump;
    }

    [PunRPC]
    public void TakeDamage(int dmg, Faction factionSource, PhotonMessageInfo info)
    {
        MyCurrentValue -= dmg;

        OnHealthChanged?.Invoke();
        DamagePopup dmgNum;
        switch (factionSource)
        {
            case Faction.Player:
                dmgNum= Instantiate(damagePopupPrefab, RandomPopupPos(), Quaternion.identity, damageDump);
                Color dmgColor = info.Sender == PhotonNetwork.LocalPlayer ? Color.white : Color.blue;
                dmgNum.Initialize(dmg, dmgColor);
                if (info.Sender == PhotonNetwork.LocalPlayer)
                {
                    unit.Select();
                }
                break;
            case Faction.Monster:
                if (photonView.IsMine)
                {
                    dmgNum = Instantiate(damagePopupPrefab, RandomPopupPos(), Quaternion.identity, damageDump);
                    dmgNum.Initialize(dmg, Color.red);
                }
                break;
            case Faction.Building:
                break;
            default:
                break;
        }


        if (MyCurrentValue <= 0)
        {
            OnDeath?.Invoke();
            Destroy(gameObject);
        }
    }

    [PunRPC]
    public void Heal(int value, PhotonMessageInfo info)
    {
        MyCurrentValue += value;

        OnHealthChanged?.Invoke();
        DamagePopup dmgNum;
        dmgNum = Instantiate(damagePopupPrefab, RandomPopupPos(), Quaternion.identity, damageDump);
        dmgNum.Initialize(value, Color.green);
    }

    private Vector2 RandomPopupPos()
    {
        return new Vector2(transform.position.x + UnityEngine.Random.Range(-0.4f, 0.4f), transform.position.y);
    }
}
