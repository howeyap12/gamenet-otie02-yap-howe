using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public abstract class Attacking : MonoBehaviourPunCallbacks
{
    [SerializeField]
    protected int attackDamage;

    protected Unit unit;

    private void Awake()
    {
        unit = GetComponent<Unit>();
    }

    public abstract void Attack();

}
